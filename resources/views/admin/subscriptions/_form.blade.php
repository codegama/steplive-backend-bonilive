<div class="row">

    <div class="col-12">

        <div class="card">

            <div class="card-body">

                 <h5 class="border-bottom pb-4 text-uppercase">

                    {{$subscription_details->id ? tr('edit_subscription') : tr('add_subscription')}}

                    <a class="btn btn-outline-primary float-right" href="{{route('admin.subscriptions.index')}}"> <i class="fa fa-eye"></i> {{tr('view_subscriptions')}} </a>

                </h5>

                <form class="forms-sample" action="{{ Setting::get('is_demo_control_enabled') == NO ? route('admin.subscriptions.save') : '#'}}" method="POST" enctype="multipart/form-data" role="form">

                    @csrf

                    <input type="hidden" name="subscription_id" id="subscription_id" value="{{$subscription_details->id}}">

                    <div class="row">

                        <div class="form-group col-md-6">

                            <label for="title" class="">{{ tr('title') }} <span class="admin-required">*</span></label>

                            <input type="text" name="title" class="form-control" id="title" value="{{ old('title') ?: $subscription_details->title }}" placeholder="{{ tr('title') }}" required >
                            
                        </div>

                        <div class="form-group col-md-6">

                            <label for="amount" class="">{{ tr('amount') }} <span class="admin-required">*</span></label>

                            <input type="number" value="{{ old('amount') ?: $subscription_details->amount }}" name="amount" class="form-control" id="amount" placeholder="{{ tr('amount') }}" min="0" step="any" required>
                        </div>
                        
                    </div>

                    <div class="row">

                        <div class="form-group col-md-6">

                            <label for="no_of_users" class="">{{ tr('no_of_users') }} <span class="admin-required">*</span></label>

                            <input type="number" name="no_of_users" class="form-control" id="no_of_users" value="{{ old('no_of_users') ?: $subscription_details->no_of_users }}" placeholder="{{ tr('no_of_users') }}" required >
                            
                        </div>

                        <div class="form-group col-md-6">

                            <label for="no_of_minutes" class="">{{ tr('no_of_minutes') }} <span class="admin-required">*</span></label>

                            <input type="number" value="{{ old('no_of_minutes') ?: $subscription_details->no_of_minutes }}" name="no_of_minutes" class="form-control" id="no_of_minutes" placeholder="{{ tr('no_of_minutes') }}" min="0" step="any" required>
                        </div>
                        
                    </div>

                    <div class="row">
                    
                        <div class="form-group col-md-6">

                            <label for="plan">{{ tr('plan') }} <span class="admin-required">*</span></label>

                            <input type="number" min="1"  max="30" required name="plan" class="form-control" id="plan" value="{{ old('plan') ?: $subscription_details->plan }}" title="{{ tr('plan') }}" placeholder="{{ tr('plan') }}">
                        </div>

                         <div class="form-group col-md-6">

                            <label for="plan_type">{{ tr('plan_type') }} <span class="admin-required">*</span></label>

                            <select class="form-control select2" id="plan_type" name="plan_type" required="">
                                <option value="">{{tr('select_plan_type')}}</option>

                                @foreach($subscription_plan_types as $subscription_plan_type)
                                    <option value="{{$subscription_plan_type}}"@if($subscription_details->plan_type == $subscription_plan_type) selected @endif>
                                        {{ucfirst($subscription_plan_type)}}
                                    </option>
                                @endforeach

                            </select>
                        </div>

                    </div>

                    <div class="row">
                    
                        <div class="form-group col-md-6" style="display: none;">

                            <div class="form-group clearfix">

                                <div class="icheck-success d-inline">
                                    <input type="checkbox" id="checkboxSuccess1" name="is_free" value="{{YES}}" @if($subscription_details->is_free ==  YES) checked="checked" @endif>
                                    <label for="checkboxSuccess1">{{tr('is_free')}}</label>

                                </div>

                            </div>

                        </div>

                         <div class="form-group col-md-6">

                            <div class="form-group clearfix">

                                <div class="icheck-success d-inline">
                                    <input type="checkbox" id="checkboxSuccess2" name="is_popular"
                                    value="{{YES}}" @if($subscription_details->is_popular ==  YES) checked="checked" @endif>
                                    <label for="checkboxSuccess2">{{tr('is_popular')}}</label>
                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="form-group col-md-12">

                            <label for="simpleMde">{{ tr('description') }}</label>

                            <textarea class="form-control" id="description" name="description">{{ old('description') ?: $subscription_details->description}}</textarea>

                        </div>

                    </div>                 

                    <button type="reset" class="btn btn-warning">{{ tr('reset')}}</button>

                    @if(Setting::get('is_demo_control_enabled') == NO )

                        <button type="submit" class="btn btn-success mr-2">{{ tr('submit') }} </button>

                    @else

                        <button type="button" class="btn btn-success mr-2" disabled>{{ tr('submit') }}</button>
                        
                    @endif

                </form>

            </div>

        </div>

    </div>

</div>



