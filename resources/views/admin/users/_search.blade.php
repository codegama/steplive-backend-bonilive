 <form  action="{{route('admin.users.index')}}" method="GET" role="search">

    <div class="row pt-2 pb-2">

        <div class="col-3">
            @if(Request::has('search_key'))
                <p class="text-muted">Search results for <b>{{Request::get('search_key')}}</b></p>
            @endif
        </div>
        
        <div class="col-3">

            <select class="form-control select2" name="status">

                <option  class="select-color" value="">{{tr('select_status')}}</option>

                <option  class="select-color" value="{{SORT_BY_APPROVED}}"  @if(Request::get('status') == SORT_BY_APPROVED)  selected @endif>{{tr('approved')}}</option>

                <option  class="select-color" value="{{SORT_BY_DECLINED}}" @if(Request::get('status') == SORT_BY_DECLINED) selected @endif>{{tr('declined')}}</option>

                <option  class="select-color" value="{{SORT_BY_EMAIL_VERIFIED}}" @if(Request::get('status') == SORT_BY_EMAIL_VERIFIED) selected @endif>{{tr('verified')}}</option>

                <option  class="select-color" value="{{SORT_BY_EMAIL_NOT_VERIFIED}}" @if(Request::get('status') == SORT_BY_EMAIL_NOT_VERIFIED) selected @endif>{{tr('un_verified')}}</option>

            </select>

        </div>

        <div class="col-6">

            <div class="input-group">
                <input type="text" class="form-control search-input" name="search_key"
                    placeholder="{{tr('users_search_placeholder')}}"> <span class="input-group-btn">
                    &nbsp

                    <button type="submit" class="btn btn-primary btn-width">
                       {{tr('search')}}
                    </button>

                    <a class="btn btn-primary" href="{{route('admin.users.index')}}">{{tr('clear')}}
                    </a>
                </span>
            </div>

        </div>

    </div>

</form>