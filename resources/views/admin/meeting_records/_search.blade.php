<form action="{{route('admin.meeting_records.index')}}" method="GET" role="search">

<div class="row pt-2 pb-2">

    <div class="col-6">
        @if(Request::has('search_key'))
        <p class="text-muted">{{tr('search_results_for')}}<b>{{Request::get('search_key')}}</b></p>
        @endif
    </div>


    <div class="col-6">

        <div class="input-group">
            <input type="text" class="form-control" value="{{Request::get('search_key')}}" name="search_key" placeholder="{{tr('meetings_search_placeholder')}}"> <span class="input-group-btn">

                &nbsp

                <button type="submit" class="btn btn-primary btn-width">
                    {{tr('search')}}
                </button>

                <a class="btn btn-primary" href="{{route('admin.meeting_records.index')}}">{{tr('clear')}}
                </a>
                </span>
            </div>

        </div>

    </div>

</form>